let pages = [];
const otherPages = document.querySelectorAll('.nav-item a');
otherPages.forEach(el => {
    pages.push(el.hash.substr(1));
})

window.onhashchange = () => {
    init();
}

window.onload = () => {
    init();
}

const init = () => {
    const page = gethash();
    if(page) {
        handleChange(page);
    }
}

const gethash = () => {
    const urlExtension = window.location.hash.slice(1);
    let page;
    
    if (!urlExtension || !pages.includes(urlExtension)) {
        window.location.hash = '#home';

    } else {
        page  = urlExtension;
    }
    return page;
}

const handleChange = (page) => {
    const allSections = document.querySelectorAll('.page');
    let changeRequired = true;
    allSections.forEach(section => {
        if(section.classList.contains('visible-section') && section.classList.contains(page)) {
            changeRequired = false;
        }
    })

    if(changeRequired) {
        clearAlert();
        hideWebSite();
    
       setTimeout(() => {
            
            hideAllSections();
            showHashSection(page);
            
       }, 200)
        setTimeout(function() {
            showWebSite();
        }, 200)
    }
}

const hideWebSite = () => {
    const webSite = document.querySelector('body');
    webSite.style.visibility = 'hidden';
    webSite.style.opacity = '0';
}


const showWebSite = () => {
    const webSite = document.querySelector('body');
    webSite.style.visibility = 'visible';
    webSite.style.opacity = '1';

}

const hideAllSections = () => {
    const allSections = document.querySelectorAll('.page');
    allSections.forEach(section => {
        if(section.classList.contains('visible-section')) {
            section.classList.remove("visible-section");
        }
        
    })
}

const showHashSection = (page) => {
    const section = document.querySelector(`.${page}`);
    section.classList.add("visible-section");
}

/*
    NAV AND HAMBURGER
*/

const navBar = document.querySelector('.navigation');
const hamburger = document.querySelector('.hamburger');
const cross = document.querySelector('.cross');
const navItem = document.querySelectorAll('.nav-item a');
const mq = window.matchMedia( "(min-width: 770px)" );
const openHamburger = () => {
    navBar.classList.add('fade-in-and-out');
    hamburger.classList.add("hidden");
    cross.classList.remove('hidden');
    navBar.classList.remove('hidden');
}

const closeHamburger = () => {
    cross.classList.add('hidden');
    hamburger.classList.remove("hidden");
    navBar.classList.add('hidden');
}
hamburger.addEventListener('click', () => {
    openHamburger();
});

cross.addEventListener('click', () => {
    closeHamburger();
});

const widthChange = (mq) => {
    if (mq.matches) {
        navBar.classList.remove('fade-in-and-out');
        hamburger.classList.add("hidden");
        cross.classList.add('hidden');
        navBar.classList.remove('vertical-menu');
        navBar.classList.remove('hidden');
        navItem.forEach((item) => {
            item.classList.remove('hide-underline');
        })
        navItem.forEach(item => {
            item.removeEventListener('click', closeHamburger);
        })
    } else {
        navBar.classList.add('hidden');
        navBar.classList.add('vertical-menu');
        hamburger.classList.remove('hidden');
        cross.classList.add('hidden');
        navItem.forEach((item) => {
            item.classList.add('hide-underline');
        })
        navItem.forEach(item => {
            item.addEventListener('click', closeHamburger);
        })
    }
}
widthChange(mq);
mq.addEventListener("change", (e) => widthChange(e));

/* RECORDING */

const youtube = document.querySelectorAll( ".youtube" );

for (var i = 0; i < youtube.length; i++) {
    const altTag = youtube[i].title;
    const source = "https://img.youtube.com/vi/"+ youtube[i].dataset.embed +"/sddefault.jpg";
    // Load the image asynchronously
    const image = new Image();
    image.src = source;
    image.alt = altTag;
    image.addEventListener( "load", function() {
        youtube[i].appendChild(image);
    }(i));
    youtube[i].addEventListener( "click", function() {
        const altTitle = this.title;
        const iframe = document.createElement("iframe");
        iframe.setAttribute("frameborder", "0");
        iframe.setAttribute("allowfullscreen", "");
        iframe.setAttribute("src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1" );
        iframe.setAttribute("title", altTitle)
        this.innerHTML = "";
        this.appendChild(iframe);
    });
}

/*
    GALLERY
*/
const focusImageContainer = document.querySelector('.focus-image-background');
const focusImage = focusImageContainer.querySelector('img');


const galleryImages = document.querySelectorAll('.gallery-image img');
galleryImages.forEach(e => {
    e.addEventListener('click', event => {
        const altTag = event.target.alt;
        const imageSource = event.target.src;
        focusImage.src = imageSource;
        focusImage.alt = altTag;
        focusImageContainer.style.visibility = 'visible';
        focusImageContainer.style.opacity = 1;
        const body = document.querySelector('body');
        body.scroll = "no";
        body.style.overflow = "hidden";
        focusImageContainer.addEventListener('click', () => {
            focusImageContainer.style.visibility = 'hidden';
            focusImageContainer.style.opacity = 0;
            body.scroll = "yes";
            body.style.overflow = "visible";
        })
    })
})

/*
    CONTACT
*/

function clearForm(name, email, message) {
    name.value = "";
    email.value = "";
    message.value = "";
}

function feedbackToUser(result) {
    if(result === "success") {
        const successMessage = document.querySelector('.alert-success');
        successMessage.classList.remove('transparent');
    } else if (result === "fail") {
        const failMessage = document.querySelector('.alert-fail');
        failMessage.classList.remove('transparent');
    } else if (result === "no-captcha") {
        const failMessage = document.querySelector('.alert-warn');
        failMessage.classList.remove('transparent');
    }
    setTimeout(clearAlert, 5000);
}

function clearAlert() {
    const alerts = document.querySelectorAll('.alert');
    alerts.forEach(alert => {
        if(!alert.classList.contains('transparent')) {
            alert.classList.add('transparent');
        }
    })
}


function processForm(e) {
    e.preventDefault();
    clearAlert();
    var URL = "https://7rauz2ow6i.execute-api.eu-west-1.amazonaws.com/pianissami/contact-us";

    const name = document.querySelector('input[id="name"]');
    const email = document.querySelector('input[id="email"]');
    var message = document.querySelector('textarea[id="message"]');
    var data = {
       name : name.value,
       email : email.value,
       message : message.value,
       captchaResponse: grecaptcha.getResponse()
    };

    if(data.captchaResponse === "" || data.captchaResponse === null) {
        feedbackToUser("no-captcha");
        return;
    }

    var xhr = new XMLHttpRequest();
    xhr.open("POST", URL, true);
    xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

    // send the collected data as JSON
    xhr.send(JSON.stringify(data));

    xhr.onload = function () {
        if(xhr.status === 200) {
            feedbackToUser("success");
        } else {
            feedbackToUser("fail");
        }
    };

    xhr.onerror = function() {
        feedbackToUser("fail");
    }

    clearForm(name, email, message);
     
  }
